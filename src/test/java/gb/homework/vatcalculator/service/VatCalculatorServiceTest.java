package gb.homework.vatcalculator.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import gb.homework.vatcalculator.BaseVatCalculator;
import gb.homework.vatcalculator.dto.CalculatorType;
import gb.homework.vatcalculator.service.method.ByPriceInclVAT;
import gb.homework.vatcalculator.service.method.ByPriceWithoutVAT;
import gb.homework.vatcalculator.service.method.ByValueAddedTax;

/**
 * Tests the VatCalculatorService.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
class VatCalculatorServiceTest extends BaseVatCalculator {

	/**
     * The service that we want to test.
     */
    @Autowired
    private VatCalculatorService service;
    
    @Autowired
    private ByPriceInclVAT byPriceInclVAT;
    
    @Autowired
    private ByPriceWithoutVAT byPriceWithoutVAT;
    
    @Autowired
    private ByValueAddedTax byValueAddedTax;

    
	@Test
	@DisplayName("Test findCalculatorType Success")
	void testFindCalculatorType() {
		
		// Execute the services calls and assert the responses
		Assertions.assertSame(service.findCalculatorType(vatByPriceInclVATRequestDTO), CalculatorType.PRICE_INCL_VAT , "findCalculatorType with vatByPriceInclVATRequestDTO should return CalculatorType.PRICE_INCL_VAT");
		Assertions.assertSame(service.findCalculatorType(vatByValueAddedTaxRequestDTO), CalculatorType.VALUE_ADDED_TAX , "findCalculatorType with vatByValueAddedTaxRequestDTO should return CalculatorType.VALUE_ADDED_TAX");
		Assertions.assertSame(service.findCalculatorType(vatByPriceWithoutVATRequestDTO), CalculatorType.PRICE_WITHOUT_VAT , "findCalculatorType with vatByPriceWithoutVATRequestDTO should return CalculatorType.PRICE_WITHOUT_VAT");
		
	}
	
	@Test
	@DisplayName("Test CallCalculatorMethod Success")
	void testCallCalculatorMethod() {
		
		// Execute the services calls and assert the responses
		Assertions.assertEquals(asJsonString(service.callCalculatorMethod(byPriceInclVAT, vatByPriceInclVATRequestDTO)), asJsonString(vatByPriceInclVATResponseDTO) , "callCalculatorMethod with vatByPriceInclVATRequestDTO should return vatByPriceInclVATResponseDTO");
		Assertions.assertEquals(asJsonString(service.callCalculatorMethod(byPriceWithoutVAT, vatByPriceWithoutVATRequestDTO)), asJsonString(vatByPriceWithoutVATResponseDTO) , "callCalculatorMethod with vatByValueAddedTaxRequestDTO should return vatByPriceWithoutVATResponseDTO");
		Assertions.assertEquals(asJsonString(service.callCalculatorMethod(byValueAddedTax, vatByValueAddedTaxRequestDTO)), asJsonString(vatByValueAddedTaxResponseDTO) , "callCalculatorMethod with vatByPriceWithoutVATRequestDTO should return vatByPriceWithoutVATResponseDTO");
		
	}

	@Test
	@DisplayName("Test calcVat Success")
	void testCalcVat() {
		
		// Execute the services calls and assert the responses
		Assertions.assertEquals(asJsonString(service.calcVat(vatByPriceInclVATRequestDTO)), asJsonString(vatByPriceInclVATResponseDTO) , "calcVat with vatByPriceInclVATRequestDTO should return vatByPriceInclVATResponseDTO");
		Assertions.assertEquals(asJsonString(service.calcVat(vatByPriceWithoutVATRequestDTO)), asJsonString(vatByPriceWithoutVATResponseDTO) , "calcVat with vatByValueAddedTaxRequestDTO should return vatByPriceWithoutVATResponseDTO");
		Assertions.assertEquals(asJsonString(service.calcVat( vatByValueAddedTaxRequestDTO)), asJsonString(vatByValueAddedTaxResponseDTO) , "calcVat with vatByPriceWithoutVATRequestDTO should return vatByPriceWithoutVATResponseDTO");
		
	}
	
}
