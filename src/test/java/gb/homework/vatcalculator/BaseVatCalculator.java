package gb.homework.vatcalculator;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.ObjectMapper;

import gb.homework.vatcalculator.dto.VatCalculatorDTO;

/**
 * Base class for tests.
 */
public class BaseVatCalculator {

	protected final VatCalculatorDTO vatByPriceWithoutVATRequestDTO = VatCalculatorDTO.builder()
													.priceWithoutVAT(new BigDecimal(100))
													.vatRate(new BigDecimal(10))
													.build();
	
	protected final VatCalculatorDTO vatByPriceWithoutVATResponseDTO = VatCalculatorDTO.builder()
				.priceWithoutVAT(new BigDecimal(100))
				.priceInclVAT(new BigDecimal("110.00"))
				.valueAddedTax(new BigDecimal("10.00"))
				.vatRate(new BigDecimal(10))
				.build();
	
	protected final VatCalculatorDTO vatByPriceInclVATRequestDTO = VatCalculatorDTO.builder()
				.priceInclVAT(new BigDecimal(1000))
				.vatRate(new BigDecimal(5))
				.build();
	
	protected final VatCalculatorDTO vatByPriceInclVATResponseDTO = VatCalculatorDTO.builder()
				.priceWithoutVAT(new BigDecimal("950.00"))
				.priceInclVAT(new BigDecimal(1000))
				.valueAddedTax(new BigDecimal("50.00"))
				.vatRate(new BigDecimal(5))
				.build();
	
	protected final VatCalculatorDTO vatByValueAddedTaxRequestDTO = VatCalculatorDTO.builder()
				.valueAddedTax(new BigDecimal(200))
				.vatRate(new BigDecimal(5))
				.build();
	
	protected final VatCalculatorDTO vatByValueAddedTaxResponseDTO = VatCalculatorDTO.builder()
				.priceWithoutVAT(new BigDecimal("4000.00"))
				.priceInclVAT(new BigDecimal("4200.00"))
				.valueAddedTax(new BigDecimal(200))
				.vatRate(new BigDecimal(5))
				.build();

    protected String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

	public BaseVatCalculator() {
		// TODO Auto-generated constructor stub
	}

}
