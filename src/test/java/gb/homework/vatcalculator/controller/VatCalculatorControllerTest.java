package gb.homework.vatcalculator.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import gb.homework.vatcalculator.BaseVatCalculator;
import gb.homework.vatcalculator.dto.VatCalculatorDTO;
import gb.homework.vatcalculator.service.VatCalculatorService;

/**
 * Tests the VatCalculatorController.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class VatCalculatorControllerTest extends BaseVatCalculator {

	private static final String THE_FILED_VAT_RATE_SHOULD_CONTAIN_A_VALID_AND_NON_ZERO_NUMERIC_VALUE = "The filed 'vatRate' should contain a valid and non zero numeric value";

	private static final String ONE_OF_PRICE_WITHOUT_VAT_PRICE_INCL_VAT_VALUE_ADDED_TAX_SHOULD_CONTAIN_A_VALID_AND_NON_ZERO_NUMERIC_VALUE = "One of: (priceWithoutVAT, priceInclVAT, valueAddedTax) should contain a valid and non zero numeric value";

	private static final String ONE_AND_ONLY_ONE_OF_PRICE_WITHOUT_VAT_PRICE_INCL_VAT_VALUE_ADDED_TAX_SHOULD_CONTAIN_A_VALID_AND_NON_ZERO_NUMERIC_VALUE_HOWEVER_MORE_THEN_ONE_WAS_PROVIDED = "One and only one of: (priceWithoutVAT, priceInclVAT, valueAddedTax) should contain a valid and non zero numeric value. However, more then one was provided";

	/**
     * A mock version of the VatCalculatorService for use in our tests.
     */
	@MockBean
	private VatCalculatorService service;
	
	@Autowired
	private MockMvc mockMvc;

	@Test
	@DisplayName("POST /calculateVat {\"priceWithoutVAT\":100,\"vatRate\":10} - Success")
	void testCalcVatByPriceWithoutVAT() throws Exception {
		
        doReturn(vatByPriceWithoutVATResponseDTO).when(service).calcVat(any());
        
        mockMvc.perform(post("/calculateVat")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(vatByPriceWithoutVATRequestDTO)))

            // Validate the response code and content type
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))

            // Validate the returned fields
            .andExpect(jsonPath("$.priceWithoutVAT", is(100)))
            .andExpect(jsonPath("$.priceInclVAT", is(110.0)))
            .andExpect(jsonPath("$.valueAddedTax", is(10.0)));        
	}

	@Test
	@DisplayName("POST /calculateVat {\"priceInclVAT\":1000,\"vatRate\":5} - Success")
	void testCalcVatByValueAddedTax() throws Exception {
		// Setup mocked service
		
		
        doReturn(vatByValueAddedTaxResponseDTO).when(service).calcVat(any());
        
        mockMvc.perform(post("/calculateVat")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(vatByValueAddedTaxRequestDTO)))

            // Validate the response code and content type
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))

            // Validate the returned fields
            .andExpect(jsonPath("$.priceWithoutVAT", is(4000.0)))
            .andExpect(jsonPath("$.priceInclVAT", is(4200.0)))
            .andExpect(jsonPath("$.valueAddedTax", is(200)));        
	}
	
	@Test
	@DisplayName("POST /calculateVat {\"valueAddedTax\":200,\"vatRate\":5} - Success")
	void testCalcVatByPriceInclVAT() throws Exception {
		// Setup mocked service
		
		
        doReturn(vatByPriceInclVATResponseDTO).when(service).calcVat(any());
        
        mockMvc.perform(post("/calculateVat")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(vatByPriceInclVATRequestDTO)))

            // Validate the response code and content type
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))

            // Validate the returned fields
            .andExpect(jsonPath("$.priceWithoutVAT", is(950.0)))
            .andExpect(jsonPath("$.priceInclVAT", is(1000)))
            .andExpect(jsonPath("$.valueAddedTax", is(50.0)));        
	}
	
	private void testCalcVatMoreThanOneInput(VatCalculatorDTO dto) throws Exception {
		mockMvc.perform(post("/calculateVat")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(dto)))
                
            // Validate the response code and content type
            .andExpect(status().isBadRequest())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        
		     // Validate the returned fields
	        .andExpect(jsonPath("$.errors[0].message", is(ONE_AND_ONLY_ONE_OF_PRICE_WITHOUT_VAT_PRICE_INCL_VAT_VALUE_ADDED_TAX_SHOULD_CONTAIN_A_VALID_AND_NON_ZERO_NUMERIC_VALUE_HOWEVER_MORE_THEN_ONE_WAS_PROVIDED)));
	}
	
	@Test
	@DisplayName("POST /calculateVat [priceInclVAT + priceWithoutVAT] - MoreThanOneInput")
	void testCalcVatMoreThanOneInputpriceInclVATAndpriceWithoutVAT() throws Exception {
		        
		testCalcVatMoreThanOneInput(VatCalculatorDTO.builder()
										.priceInclVAT(new BigDecimal(1))
										.priceWithoutVAT(new BigDecimal(2))
										.vatRate(new BigDecimal(11))
									.build());
	}

	@Test
	@DisplayName("POST /calculateVat [valueAddedTax + priceWithoutVAT] - MoreThanOneInput")
	void testCalcVatMoreThanOneInputValueAddedTaxAndPriceWithoutVAT() throws Exception {
		        
		testCalcVatMoreThanOneInput(VatCalculatorDTO.builder()
										.valueAddedTax(new BigDecimal(3))
										.priceWithoutVAT(new BigDecimal(4))
										.vatRate(new BigDecimal(12))
									.build());

	}

	@Test
	@DisplayName("POST /calculateVat [priceInclVAT + valueAddedTax] - MoreThanOneInput")
	void testCalcVatMoreThanOneInputPriceInclVATAndValueAddedTax() throws Exception {
		        
		testCalcVatMoreThanOneInput(VatCalculatorDTO.builder()
										.priceInclVAT(new BigDecimal(5))
										.valueAddedTax(new BigDecimal(6))
										.vatRate(new BigDecimal(13))
									.build());

	}
	
	@Test
	@DisplayName("POST /calculateVat [All] - MoreThanOneInput")
	void testCalcVatMoreThanOneInputAll() throws Exception {
		        
		testCalcVatMoreThanOneInput(VatCalculatorDTO.builder()
										.priceInclVAT(new BigDecimal(7))
										.valueAddedTax(new BigDecimal(8))
										.priceWithoutVAT(new BigDecimal(9))
										.vatRate(new BigDecimal(14))
									.build());

	}
	
	private void testCalcVatMissingOrInvalidAmountInput(String value) throws Exception {
		mockMvc.perform(post("/calculateVat")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{"+value+"\"vatRate\":\"10\"}"))

            // Validate the response code and content type
            .andExpect(status().isBadRequest())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        
		     // Validate the returned fields
	        .andExpect(jsonPath("$.errors[0].message", is(ONE_OF_PRICE_WITHOUT_VAT_PRICE_INCL_VAT_VALUE_ADDED_TAX_SHOULD_CONTAIN_A_VALID_AND_NON_ZERO_NUMERIC_VALUE)));
	}

	@Test
	@DisplayName("POST /calculateVat priceInclVAT [\"x\",0,null,undefined] - MissingOrInvalidAmountInput")
	void testCalcVatMissingOrInvalidAmountInputForPriceInclVAT() throws Exception {
		        
        testCalcVatMissingOrInvalidAmountInput("\"priceInclVAT\": \"x\",");
        testCalcVatMissingOrInvalidAmountInput("\"priceInclVAT\": 0,");
        testCalcVatMissingOrInvalidAmountInput("\"priceInclVAT\": null,");
        testCalcVatMissingOrInvalidAmountInput("");

	}
	
	@Test
	@DisplayName("POST /calculateVat priceWithoutVAT [\"x\",0,null,undefined] - MissingOrInvalidAmountInput")
	void testCalcVatMissingOrInvalidAmountInputForPriceWithoutVAT() throws Exception {
		        
        testCalcVatMissingOrInvalidAmountInput("\"priceWithoutVAT\": \"x\",");
        testCalcVatMissingOrInvalidAmountInput("\"priceWithoutVAT\": 0,");
        testCalcVatMissingOrInvalidAmountInput("\"priceWithoutVAT\": null,");
        testCalcVatMissingOrInvalidAmountInput("");

	}

	@Test
	@DisplayName("POST /calculateVat valueAddedTax [\"x\",0,null,undefined] - MissingOrInvalidAmountInput")
	void testCalcVatMissingOrInvalidAmountInputForValueAddedTax() throws Exception {
		        
        testCalcVatMissingOrInvalidAmountInput("\"valueAddedTax\": \"x\",");
        testCalcVatMissingOrInvalidAmountInput("\"valueAddedTax\": 0,");
        testCalcVatMissingOrInvalidAmountInput("\"valueAddedTax\": null,");
        testCalcVatMissingOrInvalidAmountInput("");

	}
			
	@Test
	@DisplayName("POST /calculateVat vatRate [\"x\",0,null,undefined] - MissingOrInvalidVatRate")
	void testCalcVatMissingOrInvalidVatRate() throws Exception {
		        
        mockMvc.perform(post("/calculateVat")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"valueAddedTax\":200,\"vatRate\":\"x\"}"))

            // Validate the response code and content type
            .andExpect(status().isBadRequest())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        
		     // Validate the returned fields
	        .andExpect(jsonPath("$.errors[0].message", is(THE_FILED_VAT_RATE_SHOULD_CONTAIN_A_VALID_AND_NON_ZERO_NUMERIC_VALUE)));

        mockMvc.perform(post("/calculateVat")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"valueAddedTax\":200,\"vatRate\":0}"))

                // Validate the response code and content type
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))

                // Validate the returned fields
                .andExpect(jsonPath("$.errors[0].message", is(THE_FILED_VAT_RATE_SHOULD_CONTAIN_A_VALID_AND_NON_ZERO_NUMERIC_VALUE)));

        mockMvc.perform(post("/calculateVat")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"valueAddedTax\":200,\"vatRate\":null}"))

            // Validate the response code and content type
            .andExpect(status().isBadRequest())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        
        	// Validate the returned fields
        	.andExpect(jsonPath("$.errors[0].message", is(THE_FILED_VAT_RATE_SHOULD_CONTAIN_A_VALID_AND_NON_ZERO_NUMERIC_VALUE)));

        
        mockMvc.perform(post("/calculateVat")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"valueAddedTax\":200}"))

            // Validate the response code and content type
            .andExpect(status().isBadRequest())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            
		     // Validate the returned fields
	        .andExpect(jsonPath("$.errors[0].message", is(THE_FILED_VAT_RATE_SHOULD_CONTAIN_A_VALID_AND_NON_ZERO_NUMERIC_VALUE)));

	}
	
	@Test
	@DisplayName("POST /calculateVat {} - MissingOrInvalidAmountInput")
	void testCalcVatMissingOrInvalidAmountInputAndMissingOrInvalidVatRate() throws Exception {
		        
		mockMvc.perform(post("/calculateVat")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}"))

            // Validate the response code and content type
            .andExpect(status().isBadRequest())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            
		     // Validate the returned fields
	        .andExpect(jsonPath("$.errors[0].message", is(THE_FILED_VAT_RATE_SHOULD_CONTAIN_A_VALID_AND_NON_ZERO_NUMERIC_VALUE)))
	        .andExpect(jsonPath("$.errors[1].message", is(ONE_OF_PRICE_WITHOUT_VAT_PRICE_INCL_VAT_VALUE_ADDED_TAX_SHOULD_CONTAIN_A_VALID_AND_NON_ZERO_NUMERIC_VALUE)));


	}
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}	

}
