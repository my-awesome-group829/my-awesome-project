package gb.homework.vatcalculator.deserializor;

import java.io.IOException;
import java.math.BigDecimal;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers.BigDecimalDeserializer;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class PositiveBigDecimalDeserializer extends BigDecimalDeserializer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4167507524050587706L;

	@Override
	public BigDecimal deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
		try {
			final BigDecimal deserialized = super.deserialize(p, ctxt);
			return deserialized.signum() == 0 ? null : deserialized;
		} catch (Exception e) {			
			return null;
		}
	}

	
}
