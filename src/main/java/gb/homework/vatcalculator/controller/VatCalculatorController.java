package gb.homework.vatcalculator.controller;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import gb.homework.vatcalculator.dto.VatCalculatorDTO;
import gb.homework.vatcalculator.service.VatCalculatorService;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@Validated
public class VatCalculatorController {

    private final VatCalculatorService vatCalculatorService;

	public VatCalculatorController(VatCalculatorService vatCalculatorService) {
		this.vatCalculatorService = vatCalculatorService;
	}
	
	@PostMapping("/calculateVat")
    public VatCalculatorDTO calcVat(@RequestBody @Valid VatCalculatorDTO vatCalculatorDTO) {
        log.debug("Start VatCalculatorController.calcVat input={}",vatCalculatorDTO);

        VatCalculatorDTO result = vatCalculatorService.calcVat(vatCalculatorDTO);
        
        log.debug("End VatCalculatorController.calcVat output={}",result);
        return result;
    }

}
