package gb.homework.vatcalculator.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MissingOrInvalidAmountInputValidator.class)
@Documented
public @interface MissingOrInvalidAmountInput {
	  String message() default "One of: (priceWithoutVAT, priceInclVAT, valueAddedTax) should contain a valid and non zero numeric value";

	  Class<?>[] groups() default { };

	  Class<? extends Payload>[] payload() default { };
}
