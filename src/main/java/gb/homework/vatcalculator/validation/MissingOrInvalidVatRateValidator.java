package gb.homework.vatcalculator.validation;

import java.math.BigDecimal;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class MissingOrInvalidVatRateValidator implements ConstraintValidator<MissingOrInvalidVatRate, BigDecimal> {

	
	@Override
	public void initialize(MissingOrInvalidVatRate constraintAnnotation) {
		ConstraintValidator.super.initialize(constraintAnnotation);		
	}

	@Override
	public boolean isValid(BigDecimal value, ConstraintValidatorContext context) {
		boolean isValid = value != null;		
		return isValid;
	}

}
