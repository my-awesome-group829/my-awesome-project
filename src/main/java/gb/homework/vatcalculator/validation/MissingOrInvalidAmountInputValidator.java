package gb.homework.vatcalculator.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import gb.homework.vatcalculator.dto.VatCalculatorDTO;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class MissingOrInvalidAmountInputValidator implements ConstraintValidator<MissingOrInvalidAmountInput, VatCalculatorDTO> {

	@Override
	public boolean isValid(VatCalculatorDTO value, ConstraintValidatorContext context) {
		boolean isValid = !(value.getPriceInclVAT() == null && value.getPriceWithoutVAT() == null && value.getValueAddedTax() == null);		
		return isValid;
	}

}
