package gb.homework.vatcalculator.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import gb.homework.vatcalculator.dto.VatCalculatorDTO;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class MoreThanOneInputValidator implements ConstraintValidator<MoreThanOneInput, VatCalculatorDTO> {

	@Override
	public boolean isValid(VatCalculatorDTO value, ConstraintValidatorContext context) {
		int counter = 0;
		if (value.getPriceInclVAT() != null) counter++;
		if (value.getValueAddedTax() != null) counter++;
		if (value.getPriceWithoutVAT() != null) counter++;
		
		boolean isValid = counter<2;		
		return isValid;
	}

}
