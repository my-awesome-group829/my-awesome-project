package gb.homework.vatcalculator.service;

import javax.validation.Valid;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import gb.homework.vatcalculator.dto.CalculatorType;
import gb.homework.vatcalculator.dto.VatCalculatorDTO;
import gb.homework.vatcalculator.service.method.VatCalculatorMethod;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Service
@Validated
@Slf4j
@AllArgsConstructor
@Getter
public class VatCalculatorService implements ApplicationContextAware {

	private ApplicationContext applicationContext;
		
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;		
	}

	public VatCalculatorDTO calcVat(@Valid VatCalculatorDTO vatCalculatorDTO) {
		
		final CalculatorType calculatorType = findCalculatorType(vatCalculatorDTO);
		final VatCalculatorMethod bean = lookup(calculatorType);
		final VatCalculatorDTO resultedVatCalculatorDTO = callCalculatorMethod(bean, vatCalculatorDTO);
		return resultedVatCalculatorDTO;
		
	}

	protected CalculatorType findCalculatorType(VatCalculatorDTO vatCalculatorDTO) {
		final CalculatorType calculatorType;
		
		if (vatCalculatorDTO.getValueAddedTax() != null) {
			calculatorType = CalculatorType.VALUE_ADDED_TAX;
		} else if (vatCalculatorDTO.getPriceInclVAT() != null)  {
			calculatorType = CalculatorType.PRICE_INCL_VAT;
		} else if (vatCalculatorDTO.getPriceWithoutVAT() != null)  {
			calculatorType = CalculatorType.PRICE_WITHOUT_VAT;
		}
		else	{			
			throw new IllegalArgumentException(String.format("Unable to find suitable CalculatorMethod based on provided VatCalculatorDTO=['%s']",vatCalculatorDTO));
		}
				
		log.info("Suitable CalculatorMethod=[{}] based on provided VatCalculatorDTO=[{}] was founded", calculatorType, vatCalculatorDTO);
		return calculatorType;
	}

	@SuppressWarnings("unchecked")
	protected VatCalculatorMethod lookup(final CalculatorType calculatorType) {
		return (VatCalculatorMethod) applicationContext.getBean(calculatorType.getClazz());
	}
	
	protected VatCalculatorDTO callCalculatorMethod(VatCalculatorMethod vatCalculatorMethod, VatCalculatorDTO vatCalculatorDTO) {
		final VatCalculatorDTO resultedVatCalculatorDTO = vatCalculatorMethod.calcVat(vatCalculatorDTO);

		log.info("callCalculatorMethod request=[{}] response=[{}]", vatCalculatorDTO, resultedVatCalculatorDTO);
		return resultedVatCalculatorDTO;
	}

	
}
