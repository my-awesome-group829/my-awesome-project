package gb.homework.vatcalculator.service.method;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.stereotype.Service;

import gb.homework.vatcalculator.dto.VatCalculatorDTO;
import lombok.NoArgsConstructor;

@Service
@NoArgsConstructor
public class ByValueAddedTax implements VatCalculatorMethod {
	
	public VatCalculatorDTO calcVat(VatCalculatorDTO input) {
		
		VatCalculatorDTO output = getVatCalculatorDTO();
		BigDecimal valueAddedTax = input.getValueAddedTax();
		BigDecimal priceWithoutVAT = (valueAddedTax.scaleByPowerOfTen(2)).divide(input.getVatRate(), 2, RoundingMode.HALF_UP);
		BigDecimal priceInclVAT =  priceWithoutVAT.add(valueAddedTax);
		
		output.setValueAddedTax(valueAddedTax);
		output.setPriceInclVAT(priceInclVAT);
		output.setPriceWithoutVAT(priceWithoutVAT);
		output.setVatRate(input.getVatRate());
		
		return output;
		
	}

}
