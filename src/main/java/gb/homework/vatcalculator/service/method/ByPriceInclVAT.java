package gb.homework.vatcalculator.service.method;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.stereotype.Service;

import gb.homework.vatcalculator.dto.VatCalculatorDTO;
import lombok.NoArgsConstructor;

@Service
@NoArgsConstructor
public class ByPriceInclVAT implements VatCalculatorMethod {

	public VatCalculatorDTO calcVat(VatCalculatorDTO input) {
		
		VatCalculatorDTO output = getVatCalculatorDTO();
		
		BigDecimal priceInclVAT =  input.getPriceInclVAT();
		BigDecimal valueAddedTax = priceInclVAT.multiply(input.getVatRate()).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
		BigDecimal priceWithoutVAT = priceInclVAT.subtract(valueAddedTax);
				
				
		output.setValueAddedTax(valueAddedTax);
		output.setPriceInclVAT(priceInclVAT);
		output.setPriceWithoutVAT(priceWithoutVAT);
		output.setVatRate(input.getVatRate());
		
		return output;
	}

}
