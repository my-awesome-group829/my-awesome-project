package gb.homework.vatcalculator.service.method;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.validation.annotation.Validated;

import gb.homework.vatcalculator.dto.VatCalculatorDTO;

@Validated
public interface VatCalculatorMethod {		
	
	VatCalculatorDTO calcVat(@Valid VatCalculatorDTO vatCalculatorDTO);
	
	@Lookup
	default VatCalculatorDTO getVatCalculatorDTO() {
		return null;
	}
		
}
