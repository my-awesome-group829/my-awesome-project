package gb.homework.vatcalculator.service.method;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.stereotype.Service;

import gb.homework.vatcalculator.dto.VatCalculatorDTO;
import lombok.NoArgsConstructor;

@Service
@NoArgsConstructor
public class ByPriceWithoutVAT implements VatCalculatorMethod {

	public VatCalculatorDTO calcVat(VatCalculatorDTO input) {
		
		VatCalculatorDTO output = getVatCalculatorDTO();
		
		BigDecimal priceWithoutVAT = input.getPriceWithoutVAT();
		BigDecimal valueAddedTax = priceWithoutVAT.multiply(input.getVatRate()).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
		BigDecimal priceInclVAT =  valueAddedTax.add(priceWithoutVAT);
				
		output.setValueAddedTax(valueAddedTax);
		output.setPriceInclVAT(priceInclVAT);
		output.setPriceWithoutVAT(priceWithoutVAT);
		output.setVatRate(input.getVatRate());
		
		return output;
				
	}

}
