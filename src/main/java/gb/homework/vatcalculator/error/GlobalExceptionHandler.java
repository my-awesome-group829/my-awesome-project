package gb.homework.vatcalculator.error;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    protected static final String EXCEPTION_MESSAGE = "Exception {}: {}";

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> uncaughtException(Exception ex, WebRequest request) {
        log.error("Exception {}: {}", new Object[]{ex.getClass(), ex.getMessage(), ex});
                                           
		List<ErrorMessage> errors = Collections.singletonList(
				ErrorMessage.builder()
					.message(INTERNAL_SERVER_ERROR.toString())
					.build()
				);
		
		ErrorResponse errorResponse = ErrorResponse.builder()
        	.errors(errors)
        	.statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
        	.build();
		
        return this.handleExceptionInternal(ex, errorResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
    
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.error("Exception {}: {}", new Object[]{ex.getClass(), ex.getMessage(), ex});
        
		List<ErrorMessage> errors = Collections.singletonList(
				ErrorMessage.builder()
					.message(HttpStatus.BAD_REQUEST.getReasonPhrase())
					.build()
				);
		
		ErrorResponse errorResponse = ErrorResponse.builder()
        	.errors(errors)
        	.statusCode(HttpStatus.BAD_REQUEST.value())
        	.build();
		
        return this.handleExceptionInternal(ex, errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}


	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
    	log.error("Exception {}: {}", new Object[]{ex.getClass(), ex.getMessage(), ex});
    	    	
		List<ErrorMessage> errors = Optional.ofNullable(ex.getAllErrors())
				.orElse(Collections.emptyList())
				.stream()
				.map(
	    			error->{
						return ErrorMessage
								.builder()
									.message(error.getDefaultMessage())
								.build();
					}
				)
				.collect(Collectors.toList());
    	
    	ErrorResponse errorResponse = ErrorResponse.builder()
            	.errors(errors)
            	.statusCode(HttpStatus.BAD_REQUEST.value())
            	.build();
    		
            return this.handleExceptionInternal(ex, errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<Object> handleValidationException(ConstraintViolationException ex, WebRequest request) {
        log.error("Exception {}: {}", new Object[]{ex.getClass(), ex.getMessage(), ex});
                        
        List<ErrorMessage> errors = ex.getConstraintViolations()
        		.stream()
        		.map((e)->{
        					return ErrorMessage.builder()
        					.message(
        							String.format(
        										e.getMessage(),
        										StreamSupport.stream(e.getPropertyPath().spliterator(), false)
        											.reduce((prev, next) -> next).orElse(null)
        									))					
        					.build();
        				})
        		.collect(Collectors.toList());
        
		ErrorResponse errorResponse = ErrorResponse.builder()
        	.errors(errors)
        	.statusCode(HttpStatus.BAD_REQUEST.value())
        	.build();
		
        return this.handleExceptionInternal(ex, errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

}
