package gb.homework.vatcalculator;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import gb.homework.vatcalculator.dto.VatCalculatorDTO;

@Configuration
public class VatCalculatorConfig {

	@Bean
	@Scope("prototype")
	public VatCalculatorDTO vatCalculatorDTO() {
		return new VatCalculatorDTO();
	}

}
