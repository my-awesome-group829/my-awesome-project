package gb.homework.vatcalculator.dto;
import gb.homework.vatcalculator.service.method.*;

public enum CalculatorType {
	VALUE_ADDED_TAX(ByValueAddedTax.class),
	PRICE_INCL_VAT(ByPriceInclVAT.class),
	PRICE_WITHOUT_VAT(ByPriceWithoutVAT.class);

	@SuppressWarnings("rawtypes")
	final private Class clazz;
	
	@SuppressWarnings("rawtypes")
	CalculatorType(Class clazz) {
		this.clazz = clazz;
	}
	
	@SuppressWarnings("rawtypes")
	public Class getClazz() {
		return clazz;
	}
}