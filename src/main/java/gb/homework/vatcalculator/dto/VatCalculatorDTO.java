package gb.homework.vatcalculator.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import gb.homework.vatcalculator.deserializor.PositiveBigDecimalDeserializer;
import gb.homework.vatcalculator.validation.MissingOrInvalidAmountInput;
import gb.homework.vatcalculator.validation.MissingOrInvalidVatRate;
import gb.homework.vatcalculator.validation.MoreThanOneInput;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@MissingOrInvalidAmountInput(message = "One of: (priceWithoutVAT, priceInclVAT, valueAddedTax) should contain a valid and non zero numeric value")
@MoreThanOneInput(message = "One and only one of: (priceWithoutVAT, priceInclVAT, valueAddedTax) should contain a valid and non zero numeric value. However, more then one was provided")
public class VatCalculatorDTO {

	@JsonDeserialize(using= PositiveBigDecimalDeserializer.class)
	private BigDecimal priceWithoutVAT;
	
	@JsonDeserialize(using= PositiveBigDecimalDeserializer.class)
	private BigDecimal priceInclVAT;
	
	@JsonDeserialize(using= PositiveBigDecimalDeserializer.class)
	private BigDecimal valueAddedTax;
	
	@MissingOrInvalidVatRate(message ="The filed 'vatRate' should contain a valid and non zero numeric value" )
	@JsonDeserialize(using= PositiveBigDecimalDeserializer.class)
	private BigDecimal vatRate;
	
	
}
