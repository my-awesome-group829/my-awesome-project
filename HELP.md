# Notes

* PositiveBigDecimalDeserializer.java - Custom deserializer. Invalid and zero values are converted to null.
* package gb.homework.vatcalculator.validation - contains custom ConstraintValidator
* GlobalExceptionHandler.java - Exception handling is done globally.

# Docker

* docker build -t myorg/myapp .
* docker run -d -p 8080:8080 myorg/myapp

# Testing

# Price without VAT:

curl --location --request POST 'localhost:8080/calculateVat' \
--header 'Content-Type: application/json' \
--data-raw '{
    "priceWithoutVAT": 100,
    "vatRate": "10"
}'

# Price incl. VAT:

curl --location --request POST 'localhost:8080/calculateVat' \
--header 'Content-Type: application/json' \
--data-raw '{
    "priceInclVAT": 1000,
    "vatRate": 5
}'

# Value-Added Tax

curl --location --request POST 'localhost:8080/calculateVat' \
--header 'Content-Type: application/json' \
--data-raw '{
    "valueAddedTax": 200,
    "vatRate": 5
}'
